# source files
SOURCES = main.cpp MainGame.cpp

#OBJS specifies which files to compile as part of the project
OBJS = main.cpp MainGame.cpp

#CC specifies which compiler we're using
CC = g++

#COMPILER_FLAGS specifies the additional compilation options we're using
# -w suppresses all warnings
COMPILER_FLAGS = -w

#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS = -lSDL2 -lGL -lGLU -lGLEW

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = supermain

#This is the target that compiles our executable
all : $(OBJS)
	$(CC) -std=c++11 $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)